
function generateListItem(name, exp_time, stock) {
    return `<li class="list-item">
                <button class="list-item-button">
                    ${name}
                    <br>
                    <p align='left'>
                        Time to expiration: <b style='color:red'>${exp_time}</b>
                    </p>
                    <p align='left'>
                        Stock: <b style="color:red">${stock}</b>
                    </p>
                </button>
            </li>`;
}

function testHandler() {
    var list = document.getElementById("left-list-ul");
    list.innerHTML += generateListItem("Frank", true, false);
}

window.onload = function(){ 
    // document.getElementById("test-button").onclick = testHandler;
    testHandler();
    testHandler();
    testHandler();
};

